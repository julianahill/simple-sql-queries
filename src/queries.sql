--rem Query 1
SELECT COUNT(deptid) "Faculty" FROM faculty WHERE deptid=1;

--rem Query 2
SELECT sname FROM student WHERE age=(SELECT MAX(age) FROM student);

--rem Query 3
SELECT x.sname, x.deptid, x.age FROM
student x, department a, faculty b, class c, enrolled d WHERE
a.dname='Computer Sciences' AND
a.deptid=b.deptid AND
b.fid=c.fid AND
c.cname=d.cname AND
d.snum=x.snum;

--rem Query 4
SELECT DISTINCT x.sname, x.deptid FROM student x, enrolled y WHERE
y.snum=x.snum OR x.age<20;

--rem Query 5
SELECT DISTINCT x.snum FROM student x, enrolled y WHERE x.snum=y.snum AND
(SELECT DISTINCT COUNT(snum) FROM enrolled WHERE cname=y.cname)>0;

--rem Query 6
SELECT DISTINCT x.fname FROM faculty x, class y WHERE x.fid=y.fid AND
(SELECT COUNT(cname) FROM class WHERE fid=y.fid AND room=y.room)>1;

--rem Query 7
SELECT y.dname, x.fname FROM faculty x, department y WHERE x.deptid=y.deptid ORDER BY y.dname ASC;

--rem Query 8
SELECT x.deptid, x.sname, x.age FROM student x, enrolled y WHERE x.snum=y.snum AND (SELECT DISTINCT COUNT(cname) FROM enrolled WHERE snum=y.snum)>1;

--rem Query 9
SELECT DISTINCT x.fid FROM faculty x, class a, enrolled b, student c WHERE
x.fid=a.fid AND
a.cname=b.cname AND
b.snum=c.snum AND
c.sname='E.Cho';

--rem Query 10
SELECT COUNT(x.snum) "ENG400" FROM student x, enrolled y WHERE
x.age<21 AND
x.snum=y.snum AND
y.cname='ENG400';
