-- Drop the table (to avoid duplication)
DROP TABLE student;
CREATE TABLE student(
       snum INTEGER NOT NULL PRIMARY KEY,
       sname VARCHAR(256),
       deptid INTEGER,
       slevel VARCHAR(256),
       age INTEGER
);

DROP TABLE class;
CREATE TABLE class(
     cname VARCHAR(256) NOT NULL PRIMARY KEY,
     meets_at DATE,
     room VARCHAR(256),
     fid INTEGER
);

DROP TABLE enrolled;
CREATE TABLE enrolled(
       snum INTEGER NOT NULL,
       cname VARCHAR(256) NOT NULL,
       CONSTRAINT enroll PRIMARY KEY (snum, cname)
);

DROP TABLE faculty;
CREATE TABLE faculty(
       fid INTEGER NOT NULL PRIMARY KEY,
       fname VARCHAR(256),
       deptid INTEGER
);
DROP TABLE department;
CREATE TABLE department(
       deptid INTEGER NOT NULL PRIMARY KEY,
       dname VARCHAR(256),
       location VARCHAR(256)
);
