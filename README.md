# Simple Oracle Database

## Overview
In this project, you'll create a simple Oracle database1 & perform some common database
operations. 

## Step 1: Create the tables
The schema for the database is as follows:

1. STUDENT(*snum: integer, sname: string, deptid: integer, slevel: string, age: integer)
2. CLASS(*cname: string, meets_at: date, room: string, fid: integer)
3. ENROLLED(*snum:integer, *cname: string)
4. FACULTY(*fid: integer, fname: string, deptid: integer)
5. DEPARTMENT (*deptid: integer, dname: string, location:string)

The fields marked with '*' are primary key.

The meaning of these relations is straightforward:
```
STUDENT contains one record per student identified by snum;
CLASS contains one record per class uniquely defined by its name; the fid field of the
class gives the instructor of the class;
ENROLLED contains a record for each student enrolled in each course;
FACULTY contains one record per faculty member uniquely identified by the fid;
DEPARTMENT contains one record per department identified by deptid.
```
Create all the key and referential integrity constraints necessary to model the
application. Make sure that your field & table names correspond to those listed above.
For this project, we’re not asking you to create domain constraints or indices on any of
the tables (although you will not lose any points for creating them). 

*Your task:*

Create a file called tables.sql, which contains five create ... statements corresponding to
the tables listed above.
If you’re in the directory containing tables.sql, you can create your database tables as
follows:
```
$ sqlplus
...
SQL> @ tables.sql
```
Remember the ‘@’ operator forces SQL to execute commands from a file. 

## Step 2: Read data files

You are given a sample data file data.sql that you can use to populate your database for
your own tests.

*Your task:*

Check that all data insertion runs without any problem.
If you’re in the directory containing data.sql, you can insert the given data as follows:
```
$ sqlplus
...
SQL> @ data.sql
```

## Step 3: Query your database

*Queries:*

Write SQL queries that answer the questions below (one query per question) and run
them on the Oracle system. The query answers should be duplicate-free, but you should
use distinct only when necessary. If you are making any assumptions, state them clearly,
and document your queries. We will run your queries on a different dataset from the one
provided with the assignment, so be careful not to hardcode values to produce correct
answers

1. For the department with deptid=1, print the number of faculty affiliated with the
department.

2. Print the names of the oldest students in the university.

3. Print the name, department id and age of the students enrolled in a class taught by
a faculty member affiliated with the Computer Sciences department (i.e. dname is
Computer Sciences).

4. Print the name and department id of the student(s) who take at least one class OR
are younger than 20.

5. Print the ids (snum) of the students who have at least one classmate.

6. Print the names of (distinct) faculty who teach 2 or more classes in the same
room. 

7. For each department, print the names of faculty affiliated with that department,
ordered by department name (ascending).

8. Print the department id, name (sname) and age of students enrolled in at least 2
different classes.

9. Print the ids of faculty who teach classes E.Cho takes.

10. For the class ENG400, print the number of students enrolled in that class, who are
younger than 21. 

*Your task:*

Create a file called queries.sql, which contains the queries listed above, in the order they
are listed (jumbling the order of queries may cost you points). Before each query i,
please put the following comment: rem Query i. If you are not able to provide a specific
query, just type “rem Query i”, where i is the query number. So, your file should look
something like this:
```
-- rem Query 1
select ...
-- rem Query 2
select .... 
```

## Step 4: Views

Here, you'll create some simple views. Create two views (Please name them `VIEWA` and
`VIEWB`) and print their contents.

* VIEWA: A view that shows the faculty name followed by the classes taught by that
faculty, ordered by faculty name.

* VIEWB: A view that shows the names of people (students and faculty) that are expected
to be present in a room each time that a relevant class is taught there. This should
be one view with both faculty and student names. A student enrolled in a class is
"expected" to be present for each session of the class, and a faculty member
teaching a class is "expected" to be present for each session of the class. Hence,
the view should contain three fields: name, room, and time.

*Your task:*

Create a file called views.sql, which contains SQL commands (create view FOO as ...) to
create the views listed above and the `SELECT` statements that list all the data of both
views, i.e. your file should contain two view creation statements followed by two query
statements (all of which are ended with semicolons). 
